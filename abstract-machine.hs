data Expr = Val Int | Add Expr Expr | Mult Expr Expr

type Cont = [Op]
data Op = PLUS Expr | ADD Int | MULT Expr | TIMES Int


value :: Expr -> Int
value e = eval e []

eval :: Expr -> Cont -> Int
eval (Val n)    c = exec c n
eval (Add x y)  c = eval x (PLUS y : c)
eval (Mult x y) c = eval x (MULT y : c)

exec :: Cont -> Int -> Int
exec []            n = n
exec (PLUS y : c)  n = eval y (ADD n : c)
exec (ADD n : c)   m = exec c (n + m)
exec (MULT y : c)  n = eval y (TIMES n : c)
exec (TIMES n : c) m = exec c (n * m)
