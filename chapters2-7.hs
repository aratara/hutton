import Data.Char

-- ex2.3
n = a `div` (length xs)
    where
      a = 10
      xs = [1, 2, 3, 4, 5]

-- ex2.4
last_1 xs = head (reverse xs)

last_2 xs = xs !! idx
            where
              idx = (length xs) - 1

-- ex2.5
init_1 xs = reverse (tail (reverse xs))

init_2 [x] = []
init_2 (x:xs) = [x] ++ init_2 xs


-- ex3.2
bools = [True, False, True]
nums = [[1], [2], [3]]

add :: Int -> Int -> Int -> Int
add x y z = x + y + z

copy :: a -> (a, a)
copy x = (x, x)

apply :: (a -> b) -> a -> b
apply f a = f a


-- ex3.3
second :: [a] -> a
second xs = head (tail xs)

swap :: (a, b) -> (b, a)
swap (a, b) = (b, a)

pair :: x -> y -> (x, y)
pair x y = (x, y)

double :: Num a => a -> a
double a = a * 2

palindrome :: Eq a => [a] -> Bool
palindrome xs = reverse xs == xs

twice :: (a -> a) -> a -> a
twice f x = f (f x)

-- ex4.1
halve :: [a] -> ([a],[a])
halve xs = (first_half, second_half)
           where
             mid = (length xs) `div` 2
             first_half = (take mid xs)
             second_half = (drop mid xs)


-- ex4.2
third :: [a] -> a
third xs = head (tail (tail xs))
-- third xs = xs !! 2
-- third [_, _, a] = a


-- ex4.3
safetail :: [a] -> [a]
safetail xs = if null xs then xs else tail xs
{-
safetail xs | null xs   = xs
            | otherwise = tail xs

safetail [] = []
safetail (x:xs) = xs
-}

-- ex4.7
mult :: Int -> Int -> Int -> Int
mult = \x -> (\y -> (\z -> x * y * z))

-- ex4.8
luhnDouble :: Int -> Int
luhnDouble n = if (double > 9) then (double - 9) else double
                where
                  double = 2*n

luhn :: Int -> Int -> Int -> Int -> Bool
luhn a b c d = (summed `mod` 10) == 0
                    where
                      summed = sum [luhnDouble a, b, luhnDouble c, d]

-- ex5.1
-- sum [x * x | x <- [1..100]]

-- ex5.2
grid :: Int -> Int -> [(Int, Int)]
grid a b = [(x, y) | x <- [0..a], y <- [0..b]]

-- ex5.3
square :: Int -> [(Int, Int)]
square n = [(x, y) | (x, y) <- (grid n n), x /= y]

-- ex5.4
replicate' :: Int -> a -> [a]
replicate' n a = [a | _ <- [1..n]]

-- ex5.5
pyths :: Int -> [(Int, Int, Int)]
pyths n = [(x, y, z) | x <- [1..n], y <- [1..n], z <- [1..n], x*x + y*y == z*z]

-- ex5.6
factors :: Int -> [Int]
factors n = [x | x <- [1..n], n `mod` x == 0]

perfects :: Int -> [Int]
perfects n = [x | x <- [1..n], sum (init (factors x)) == x]

-- ex5.7
-- concat [[(x, y) | x <- [1,2]] | y <- [3,4]]

-- ex5.8
find :: Eq a => a -> [(a,b)] -> [b]
find k t = [v | (k', v) <- t, k == k']

positions :: Eq a => a -> [a] -> [Int]
positions x xs = find x (zip xs [0..])

-- ex5.9
scalarproduct :: [Int] -> [Int] -> Int
scalarproduct xs ys = sum [x * y | (x,y) <- zip xs ys]



-- ex 6.1
fac :: Int -> Int
fac 0 = 1
fac n | n > 0 = n * fac (n-1)
      | otherwise = error "factorial can only take non negative arguments"


-- ex6.2
sumdown :: Int -> Int
sumdown 0 = 0
sumdown n | n > 0 = n + sumdown (n-1)
          | otherwise = error "sumdown only takes non negative integers for arguments"

-- ex6.3
{-
(^) :: (Fractional a, Integral b) => a -> b -> a
m ^ 0 = 1
m ^ n = m * (m ^ (n-1))
-}


-- ex6.4
euclid :: Int -> Int -> Int
euclid a b | a == b = a
           | a < b  = euclid a (b - a)
           | otherwise = euclid (a - b) b

-- ex6.6
-- a
and' :: [Bool] -> Bool
and' (x:xs) | x == False = False
            | null xs    = True
            | otherwise  = and' xs

-- b
concat' :: [[a]] -> [a]
concat' []     = []
concat' (x:xs) = x ++ concat' xs

-- c
replicate'' :: Int -> a -> [a]
replicate'' 0 x = []
replicate'' n x = x : (replicate'' (n-1) x)

-- d
{-

(!!) :: [a] -> Int -> a
(x:xs) !! 0 = x
(_:xs) !! n | n > 0 = xs !! (n-1)
            | otherwise = error "negative arguments not allowed"
-}

-- e
elem' :: Eq a => a -> [a] -> Bool
elem' _ [] = False
elem' a (x:xs) | x == a = True
               | otherwise = elem' a xs


-- ex6.7
merge :: Ord a => [a] -> [a] -> [a]
merge xs [] = xs
merge [] ys = ys
merge (x:xs) (y:ys) | x <= y = x : merge xs (y:ys)
                    | otherwise = y : merge (x:xs) ys

-- ex 6.8
msort :: Ord a => [a] -> [a]
msort [a] = [a]
msort xs = merge lowerSorted upperSorted
           where
             (lower, upper) = halve xs
             lowerSorted = msort lower
             upperSorted = msort upper

-- ex6.9
-- a

sum' :: [Int] -> Int
sum' [x] = x
sum' (x:xs) = x + sum' xs

-- b
take' :: Int -> [a] -> [a]
take' 0 _      = []
take' _ []     = []
take' n (x:xs) = x : take' (n-1) xs

-- c
last' :: [a] -> a
last' [x] = x
last' (_:xs) = last' xs


-- ex7.1
{-
[f x | x <- xs, p x]
map f (filter p) xs
-}

-- ex7.2
-- a
all' :: (a -> Bool) -> [a] -> Bool
all' p = foldr (\x y -> p x && y) True

-- b
any' :: (a -> Bool) -> [a] -> Bool
any' _ [] = False
any' p (x: xs) = p x || any' p xs

-- c
takeWhile' :: (a -> Bool) -> [a] -> [a]
takeWhile' _ [] = []
takeWhile' p (x:xs) | p x = x : takeWhile' p xs
                    | otherwise = takeWhile' p xs

-- d
dropWhile' :: (a -> Bool) -> [a] -> [a]
dropWhile' p xs = takeWhile' (not . p) xs

-- ex7.3
map' :: (a -> b) -> [a] -> [b]
map' f = foldr (\x xs -> f x : xs) []

filter' :: (a -> Bool) -> [a] -> [a]
filter' p = foldr (\x xs -> if p x then x : xs else xs) []

-- ex7.4
dec2int :: [Int] -> Int
dec2int = foldl (\x xs -> (x * 10) + xs) 0

-- ex7.5
curry' :: ((a, b) -> c) -> a -> b -> c
curry' f = \x -> \y -> f (x, y)

uncurry' :: (a -> b -> c) -> (a, b) -> c
uncurry' f = \(x, y) -> f x y

-- ex7.6
type Bit = Int

int2bin :: Int -> [Bit]
{-
int2bin 0 = []
int2bin n = n `mod` 2 : int2bin (n `div` 2)
-}

unfold p h t x | p x       = []
               | otherwise = h x : unfold p h t (t x)

int2bin = unfold (==0) (`mod` 2) (`div` 2)

chop8 :: [Bit] -> [[Bit]]
{-
chop8 [] = []
chop8 bits = take 8 bits : chop8 (drop 8 bits)
-}
chop8 = unfold null (take 8) (drop 8)

map'' :: (a -> b) -> [a] -> [b]
map'' f = unfold null (f . head) tail

iterate' :: (a -> a) -> a -> [a]
iterate' f = unfold (\x -> False) id f

-- ex7.9
altMap :: (a -> b) -> (a -> b) -> [a] -> [b]
altMap f g xs = [ h x | (x, h) <- zip xs (concat (repeat [f,g])) ]

-- ex7.0
{-
luhn :: Int -> Int -> Int -> Int -> Bool
luhn a b c d = (summed `mod` 10) == 0
                    where
                      summed = sum [luhnDouble a, b, luhnDouble c, d]
-}

luhn' :: [Int] -> Bool
luhn' = (==0) . (`mod` 10) . sum . (altMap luhnDouble id)
