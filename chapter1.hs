qsort []     = []
qsort (x:xs) = qsort smaller ++ [x] ++ qsort larger
               where
                 smaller = [a | a <- xs, a <= x]
                 larger =  [b | b <- xs, b > x]
                 

seqn []         = return []
seqn (act:acts) = do x <- act
                     xs <- seqn acts
                     return (x:xs)

-- ex1.3
product' []     = 1
product' (x:xs) = x * product' xs

-- ex1.4
-- to obtain reversed order in qsort just switch larger and smaller:
-- qsort larger ++ [x] ++ qsort smaller


-- ex1.5
-- qsort: if you have < instead of <= you will miss on values equal to your pivot

