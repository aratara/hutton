type Pos = (Int, Int)
type Trans = Pos -> Pos

type Pair a = (a, a)

type Assoc k v = [(k, v)]

find :: Eq k => k -> Assoc k v -> v
find k t = head [v | (k', v) <- t, k == k']

data Move = North | South | East | West deriving Show

move :: Move -> Pos -> Pos
move North (x, y) = (x, y + 1)
move South (x, y) = (x, y - 1)
move East  (x, y) = (x + 1, y)
move West  (x, y) = (x - 1, y)

moves :: [Move] -> Pos -> Pos
moves [] p = p
moves (m:ms) p = moves ms (move m p)

rev :: Move -> Move
rev North = South
rev South = North
rev East  = West
rev West  = East

data Shape = Circle Float | Rect Float Float

square :: Float -> Shape
square n = Rect n n

area :: Shape -> Float
area (Circle r) = pi * r^2
area (Rect x y) = x * y

safediv :: Int -> Int -> Maybe Int
safediv _ 0 = Nothing
safediv m n = Just (m `div` n)

safehead :: [a] -> Maybe a
safehead [] = Nothing
safehead xs = Just (head xs)


data Nat = Zero | Succ Nat

nat2int :: Nat -> Int
nat2int Zero     = 0
nat2int (Succ n) = 1 + nat2int n

int2nat :: Int -> Nat
int2nat 0 = Zero
int2nat n = Succ (int2nat (n-1))

add :: Nat -> Nat -> Nat
add Zero n = n
add (Succ m) n = Succ (add m n)

-- ex8.1
mult :: Nat -> Nat -> Nat
mult Zero n = Zero
mult (Succ Zero) n = n
mult (Succ m) n = add n (mult m n)


data List a = Nil | Cons a (List a)

len :: List a -> Int
len Nil = 0
len (Cons _ xs) = 1 + len xs


data Tree a = Leaf a | Node (Tree a) a (Tree a)

{-
occurs :: Eq a => a -> Tree a -> Bool
occurs v (Leaf v')      = v == v'
occurs v (Node l v' r) = or [v' == v, occurs v l, occurs v r]
-}

flatten :: Tree a -> [a]
flatten (Leaf v) = [v]
flatten (Node l v r) = flatten l ++ [v] ++ flatten r

-- for search trees
occurs :: Ord a => a -> Tree a -> Bool
occurs v (Leaf v')                = v == v'
occurs v (Node l v' r) | v == v'   = True
                      | v < v'    = occurs v l
                      | otherwise = occurs v r

-- ex8.2
occurs' :: Ord a => a -> Tree a -> Bool
occurs' v (Leaf v')                      = v == v'
occurs' v (Node l v' r) | ordering == EQ = True
                        | ordering == LT = occurs' v l
                        | otherwise      = occurs' v r
                        where
                          ordering = compare v v'

-- ex8.3
data Tree' a = Leaf' a | Node' (Tree' a) (Tree' a) deriving Show

leaves :: Tree' a -> [a]
leaves (Leaf' v) = [v]
leaves (Node' l r) = leaves l ++ leaves r

balanced :: Tree' a -> Bool
balanced (Leaf' _) = True
balanced (Node' l r) = diff == 0 || diff == -1 || diff == 1
                       where
                         diff = (length (leaves l)) - (length (leaves r))

-- ex8.4
halve :: [a] -> ([a],[a])
halve xs = (half1, half2)
           where
             half = (length xs) `div` 2
             half1 = (take half xs)
             half2 = (drop half xs)
             
balance :: [a] -> Tree' a
balance [v] = Leaf' v
balance vs  = (Node' (balance half1) (balance half2))
              where
                (half1, half2) = halve vs

-- ex8.5
data Expr = Val Int | Add Expr Expr

folde :: (Int -> a) -> (a -> a -> a) -> Expr -> a
folde f g (Val v) = f v
folde f g (Add e e') = g (folde f g e) (folde f g e')

-- ex8.6
eval' :: Expr -> Int
eval' = folde id (+)

size :: Expr -> Int
size = folde (\_ -> 1) (+)

-- ex8.7
data Maybe' a = Nothing' | Just' a
instance Eq a => Eq (Maybe' a) where
  Nothing' == Nothing' = True
  Just' x  == Just' y  = x == y
  _        == _        = False

{-
instance Eq a => Eq [a] where
  [] == []         = True
  (x:xs) == (y:ys) = x == y && xs == ys
-}

-- ex8.9
value :: Expr -> Int
value e = eval e []
{- value (Val n) = n
   value (Add x y) = value x + value y
-}

type Cont = [Op]
data Op = EVAL Expr | ADD Int

eval :: Expr -> Cont -> Int
eval (Val n)   c = exec c n
eval (Add x y) c = eval x (EVAL y : c)

exec :: Cont -> Int -> Int
exec []           n = n
exec (EVAL y : c) n = eval y (ADD n : c)
exec (ADD n : c)  m = exec c (n + m)
