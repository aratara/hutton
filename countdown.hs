import Data.List (sortBy)

data Op = Add | Sub | Mul | Div

instance Show Op where
  show Add = "+"
  show Sub = "-"
  show Mul = "*"
  show Div = "/"

valid :: Op -> Int -> Int -> Bool
valid Add _ _ = True
valid Mul _ _ = True
valid Sub x y = x > y
valid Div x y = x `mod` y == 0

apply :: Op -> Int -> Int -> Int
apply Add x y = x + y
apply Sub x y = x - y
apply Mul x y = x * y
apply Div x y = x `div` y

data Expr = Val Int | App Op Expr Expr

instance Show Expr where
  show (Val n)     = show n
  show (App o l r) = brak l ++ show o ++ brak r
                     where
                       brak (Val n) = show n
                       brak e       = "(" ++ show e ++ ")"

values :: Expr -> [Int]
values (Val n)     = [n]
values (App _ l r) = values l ++ values r

eval :: Expr -> [Int]
eval (Val n)     = [n | n > 0]
eval (App o l r) = [apply o x y | x <- eval l,
                                  y <- eval r,
                                  valid o x y]

-- | returns all subsequences of a list
subs :: [a] -> [[a]]
subs []     = [[]]
subs (x:xs) = yss ++ map (x:) yss
              where yss = subs xs


-- | returns all possible ways of inserting a new element into a list
interleave :: a -> [a] -> [[a]]
interleave x []     = [[x]]
interleave x (y:ys) = (x:y:ys) : map (y:) (interleave x ys)


-- | returns all permutations of a list, which are given by all possible reorderings
perms :: [a] -> [[a]]
perms [] = [[]]
perms (x:xs) = concat (map (interleave x) (perms xs))

-- | returns all possible ways of selecting zero or more elements in any order
choices :: [a] -> [[a]]
choices = concat . map perms . subs


isSolution :: Expr -> [Int] -> Int -> Bool
isSolution e ns n =
  elem (values e) (choices ns) && eval e == [n]


-- brute forcing

-- | returns all possible ways of splitting a list into two non-empty lists
-- that append to give the original list
split :: [a] -> [([a], [a])]
split [] = []
split [_] = []
split (x:xs) = ([x], xs) : [(x:ls, rs) | (ls, rs) <- split xs]


exprs :: [Int] -> [Expr]
exprs [] = []
exprs [n] = [Val n]
exprs ns = [e | (ls, rs) <- split ns,
                l        <- exprs ls,
                r        <- exprs rs,
                e        <- combine l r]

combine :: Expr -> Expr -> [Expr]
combine l r = [App o l r | o <- ops]

ops :: [Op]
ops = [Add, Sub, Mul, Div]

solutions :: [Int] -> Int -> [Expr]
solutions ns n =
  [e | ns' <- choices ns, e <- exprs ns', eval e == [n]]

-- combining generation and evalution of exprs
-- exploiting algebraic properties

valid' :: Op -> Int -> Int -> Bool
valid' Add x y = x <= y
valid' Sub x y = x > y
valid' Mul x y = x /= 1 && y /= 1 && x <= y
valid' Div x y = y /= 1 && x `mod` y == 0

type Result = (Expr, Int)

results :: [Int] -> [Result]
results [] = []
results [n] = [(Val n, n) | n > 0]
results ns = [res | (ls, rs) <- split ns,
                    lx       <- results ls,
                    ry       <- results rs,
                    res      <- combine' lx ry]

combine' :: Result -> Result -> [Result]
combine' (l,x) (r,y) =
  [(App o l r, apply o x y) | o <- ops, valid' o x y]

solutions' :: [Int] -> Int -> [Expr]
solutions' ns n =
  [e | ns' <- choices ns, (e,m) <- results ns', m == n]
  
-- ex9.1
choices' :: [a] -> [[a]]
choices' ns = [ns' | s <- subs ns, ns' <- perms s]

-- ex9.2
rmFirst :: Eq a => a -> [a] -> [a]
rmFirst _ [] = []
rmFirst y (x:xs) | x == y = xs
                 | otherwise = x : rmFirst y xs

isChoice :: Eq a => [a] -> [a] -> Bool
isChoice [] _ = True
isChoice (x:xs) ys | elem x ys = isChoice xs (rmFirst x ys)
                   | otherwise = False

-- ex9.3
-- If split also returned pairs containing empty lists, then exprs would never terminate, and hence solutions
-- would never terminate.

-- ex9.4
cdlist :: [Int]
cdlist = [1,3,7,10,25,50]

allExprs :: [Int] -> [Expr]
allExprs =  concat . map exprs . choices

validExprs :: [Int] -> [[Int]]
validExprs = filter (not . null) . map eval . allExprs

totalExprs = length (allExprs cdlist) -- 33,665,406
totalSuccessful = length (validExprs cdlist) -- naniii? only 245644? Due to use of algebraic properties in valid?

-- ex9.6b
resAcc :: [Result] -> Result -> [Result]
resAcc [] y = [y]
resAcc (x:xs) y | (snd x) == (snd y) = y:x:xs
                | (snd y) < (snd x)  = [y]
                | otherwise          = x:xs

dummyRes :: Result
dummyRes = (Val 0, maxBound)

solutions'' :: [Int] -> Int -> [Result]
solutions'' ns n = foldl resAcc [dummyRes] [(e,abs (m - n)) | ns' <- choices ns, (e,m) <- results ns']

--ex9.6c
complexity :: Expr -> Int
complexity = length . values

instance Eq Expr where
  e == e' = show e == show e'

instance Ord Expr where
  e < e' = complexity e < complexity e'
  e <= e' = complexity e <= complexity e'
  e > e' = complexity e > complexity e'
  e >= e' = complexity e >= complexity e'

solutions''' :: [Int] -> Int -> [Result]
solutions''' ns n = sortBy compare (solutions'' ns n)
